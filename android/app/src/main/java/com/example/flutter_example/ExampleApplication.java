package com.example.flutter_example;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import io.flutter.app.FlutterApplication;
import io.imox.deviceinfo.DeviceInfo;

public class ExampleApplication extends FlutterApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("ExampleApplication", "App initializing");
        DeviceInfo.getInstance()
                .setIdBrand(8888)
                .setTimeRecolecData(1, TimeUnit.DAYS)
                .load(this);
    }
}
